#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

php='php8.0'

apt -y install \
$php \
$php-apcu \
$php-bcmath \
$php-bz2 \
$php-cli \
$php-common \
$php-curl \
$php-dba \
$php-dev \
$php-fpm \
$php-gd \
$php-http \
$php-imagick \
$php-imap \
$php-intl \
$php-ldap \
$php-mailparse \
$php-mbstring \
$php-mysql \
$php-oauth \
$php-odbc \
$php-opcache \
$php-pgsql \
$php-readline \
$php-raphf \
$php-snmp \
$php-soap \
$php-solr \
$php-sqlite3 \
$php-ssh2 \
$php-tidy \
$php-uuid \
$php-xdebug \
$php-xml \
$php-xmlrpc \
$php-xsl \
$php-yaml \
$php-zip

a2enconf $php-fpm

rm -f ${0}