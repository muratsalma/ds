#!/usr/bin/env bash

# run apache as docker host uid and gid
usermod -u $APACHE_RUN_UID www-data
groupmod -g $APACHE_RUN_GID www-data

echo "disabling xdebug for all php versions"
phpdismod -v ALL xdebug

mkdir /run/php
echo "starting php-fpm service"

# startting fpm via service yields different environment vars
#service php7.1-fpm start
#service php7.3-fpm start
#service php7.4-fpm start

# start php7.1-fpm
/usr/sbin/php-fpm7.1 --nodaemonize --fpm-config /etc/php/7.1/fpm/php-fpm.conf &
/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/7.1/fpm/pool.d/www.conf 71 &

# start php7.3-fpm
/usr/sbin/php-fpm7.3 --nodaemonize --fpm-config /etc/php/7.3/fpm/php-fpm.conf &
/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/7.3/fpm/pool.d/www.conf 73 &

# start php7.4-fpm
/usr/sbin/php-fpm7.4 --nodaemonize --fpm-config /etc/php/7.4/fpm/php-fpm.conf &
/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/7.4/fpm/pool.d/www.conf 74 &

# start php8.0-fpm
/usr/sbin/php-fpm8.0 --nodaemonize --fpm-config /etc/php/8.0/fpm/php-fpm.conf &
/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/8.0/fpm/pool.d/www.conf 80 &



source /etc/apache2/envvars

a2ensite php71.ds.conf
a2ensite php71-ssl.ds.conf
a2ensite php73.ds.conf
a2ensite php73-ssl.ds.conf
a2ensite php74.ds.conf
a2ensite php74-ssl.ds.conf
a2ensite php80.ds.conf
a2ensite php80-ssl.ds.conf

echo "starting apache"
#service apache2 start
apachectl -DFOREGROUND
#tail -f /dev/null
