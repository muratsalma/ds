#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

apt update && apt upgrade -y

apt -y install \
software-properties-common \
python3-software-properties

add-apt-repository ppa:ondrej/php -y

apt -y install \
python3-pip \
git \
apache2 \
libapache2-mod-fcgid

a2enmod \
actions \
alias \
proxy \
proxy_fcgi \
setenvif \
rewrite \
ssl

a2ensite \
default-ssl.conf

apt -y install \

ffmpeg \
ghostscript

echo "setup complete, removing ${0} from docker image"
rm -f ${0}