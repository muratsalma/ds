#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

apt -y install \
ffmpeg \
ghostscript \
libjpeg-progs \
jpegoptim \
pngquant \
optipng \
pngcrush \
advancecomp

rm -f ${0}