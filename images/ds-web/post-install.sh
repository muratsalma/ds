#!/bin/bash

PHP71_FPM_INI_PATH=/etc/php/7.1/fpm/php.ini
sed -i "/^memory_limit/c\memory_limit = 512M" $PHP71_FPM_INI_PATH
sed -i "/^post_max_size/c\post_max_size = 512M" $PHP71_FPM_INI_PATH
sed -i "/^upload_max_filesize/c\upload_max_filesize = 512M" $PHP71_FPM_INI_PATH
sed -i "/^max_file_uploads/c\max_file_uploads = 500" $PHP71_FPM_INI_PATH
sed -i "/^;date.timezone/c\date.timezone = Europe\/Istanbul" $PHP71_FPM_INI_PATH
sed -i "/^;max_input_vars/c\max_input_vars = 100000" $PHP71_FPM_INI_PATH
sed -i "/^display_errors/c\display_errors = On" $PHP71_FPM_INI_PATH


PHP73_FPM_INI_PATH=/etc/php/7.3/fpm/php.ini
sed -i "/^memory_limit/c\memory_limit = 512M" $PHP73_FPM_INI_PATH
sed -i "/^post_max_size/c\post_max_size = 512M" $PHP73_FPM_INI_PATH
sed -i "/^upload_max_filesize/c\upload_max_filesize = 512M" $PHP73_FPM_INI_PATH
sed -i "/^max_file_uploads/c\max_file_uploads = 500" $PHP73_FPM_INI_PATH
sed -i "/^;date.timezone/c\date.timezone = Europe\/Istanbul" $PHP73_FPM_INI_PATH
sed -i "/^;max_input_vars/c\max_input_vars = 100000" $PHP73_FPM_INI_PATH
sed -i "/^display_errors/c\display_errors = On" $PHP73_FPM_INI_PATH

PHP74_FPM_INI_PATH=/etc/php/7.4/fpm/php.ini
sed -i "/^memory_limit/c\memory_limit = 512M" $PHP74_FPM_INI_PATH
sed -i "/^post_max_size/c\post_max_size = 512M" $PHP74_FPM_INI_PATH
sed -i "/^upload_max_filesize/c\upload_max_filesize = 512M" $PHP74_FPM_INI_PATH
sed -i "/^max_file_uploads/c\max_file_uploads = 500" $PHP74_FPM_INI_PATH
sed -i "/^;date.timezone/c\date.timezone = Europe\/Istanbul" $PHP74_FPM_INI_PATH
sed -i "/^;max_input_vars/c\max_input_vars = 100000" $PHP74_FPM_INI_PATH
sed -i "/^display_errors/c\display_errors = On" $PHP74_FPM_INI_PATH


echo "setup completed, removing /setup-image.sh from docker image"
rm -f /setup-image.sh
